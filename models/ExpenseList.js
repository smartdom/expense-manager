const mongoose = require('mongoose');
// const Expense = require('./Expense')

const expenseListSchema = new mongoose.Schema({
  name: String,
  owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  collaborators: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  expenses: [{
    name: String,
    category: String,
    amount: Number,
    comment: String,
    createdAt: Date,
    receipt: {data: Buffer, contentType: String}
  }],
  comment: String
}, { timestamps: true });

const ExpenseList = mongoose.model('ExpenseList', expenseListSchema);

module.exports = ExpenseList;
