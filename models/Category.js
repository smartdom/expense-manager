const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    name: String,
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
}, { timestamps: true });

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;