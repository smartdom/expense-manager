const _ = require('lodash');
const fs = require('fs');
const mongoose = require('mongoose');
const Category = require('../models/Category');
const ExpenseList = require('../models/ExpenseList');
const User = require('../models/User');


/**
 * GET /expenses/lists
 * Get list of expenses
 */
exports.getExpensesLists = (req, res) => {
  const userId = req.user._id;


  ExpenseList.find({}).or([{ owner: userId }, { collaborators: userId }]).sort({ 'expenses.createdAt': -1 }).exec((err, lists) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      return res.redirect('/admin');
    }
    res.render('expenses/lists/list', {
      title: 'Expenses',
      expenseLists: lists
    });
  });
};

/**
 * GET /expenses/lists/new
 * Create new expense list
 */
exports.getNewExpenseList = (req, res) => {
  res.render('expenses/lists/new', {
    title: 'Expenses'
  });
};


/**
 * POST /expenses/lists
 * Create new expense list
 */
exports.postNewExpenseList = (req, res) => {
  const list = new ExpenseList({ name: req.body.name, owner: req.user._id, comment: req.body.comment });
  list.save((err) => {
    if (err) {
      req.flash('errors', { msg: err.message });
    } else {
      req.flash('success', { msg: 'successfully added new expense list' });
    }
    return res.redirect('/lists');
  });
};

/**
 * GET /expenses/lists/:listId/delete
 * Delete expense list
 */
exports.getDeleteExpenseList = (req, res) => {
  const id = req.params.listId;
  ExpenseList.findOne({ _id: id, owner: req.user._id }, (err, list) => {
    if (err) {
      req.flash('errors', { msg: err.message });
    } else if (list != null ) {
      list.remove();
      req.flash('success', { msg: 'successfully removed expense list' });
    } else {
      req.flash('errors', { msg: 'You\'re unauthorized to remove shared list' });
    }
    return res.redirect('/lists');
  });
};

/**
 * GET /expenses/lists/:listId
 * Get expense list details
 */
exports.getExpenseListDetails = (req, res) => {
  const id = req.params.listId;
  const userId = req.user._id;

  const itemsPerPage = 10;
  

  ExpenseList.aggregate([
    { $match: { _id: mongoose.Types.ObjectId(id) } },
    { $unwind: '$expenses' },
    {
      $count: "expenses"
    }
 ], function(err, doc) {
  //console.log(err)
  //console.log(doc)
  var totalExpenses = doc[0].expenses
  var pagesNo = Math.ceil(totalExpenses / itemsPerPage)
  var currentPage = req.query.page || 0
  //console.log(currentPage)



  ExpenseList.findOne({ _id: id }).or([{ owner: userId }, { collaborators: userId }]).populate({ path: 'owner' }).populate({ path: 'collaborators' })
  .exec((err, list) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      res.redirect('/lists');
    } else {
      var amountTotal = 0;
      list.expenses.forEach((element) => {
        amountTotal += element.amount;
      });
      res.render('expenses/lists/details', {
        title: 'Expenses',
        data: list,
        total: amountTotal,
        pages: pagesNo,
        pageCurrent: currentPage
      });
    }
  });
 });
 


};

/**
 * POST /expenses/lists/:listId
 * Add new expense to list
 */
exports.postAddNewExpenseToList = (req, res) => {
  const lid = req.params.listId;

  ExpenseList.findOne({ _id: lid }, (err, list) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      res.redirect(`/lists/${lid}`);
    } else {
      if (!list.expenses) {
        list.expenses = [];
      }
      var receipt;

      if(req.file && req.file.size > 0){
        var imageData = fs.readFileSync(req.file.path);
        receipt = {contentType: req.file.mimetype, data: imageData};
      }

      // TODO: add category
      const element = { name: req.body.name, amount: req.body.amount, category: req.body.category, createdAt: new Date(), receipt: receipt };

      list.expenses.push(element);
      list.save();

        req.flash('success', { msg: 'Item added to expense list' });
        res.redirect(`/lists/${lid}`);
    }
  });
};


/**
 * GET /expenses/lists/:listId
 * Display new expense form
 */
exports.getAddNewExpenseToList = (req, res) => {
  const id = req.params.listId;
  Category.find({}, (err, list) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      res.redirect(`/lists/${id}`);
    } else {
      res.render('expenses/add', {
        title: 'Add an expense',
        categories: list,
        listId: id
      });
    }
  });
};


/**
 * GET /expenses/:expenseId/edit
 * Display new expense form
 */
exports.getEditExpense = (req, res) => {
  const lid = req.params.listId;
  const eid = req.params.expenseId;
  Category.find({}, (err, categories) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      res.redirect(`/lists/${lid}`);
    } else {
      ExpenseList.findOne({'expenses._id': eid}, { 'expenses.$': 1 }, (err, expenses) => {
        if(err){
          req.flash('errors', { msg: err.message });
          res.redirect(`/lists/${lid}`);
        } else {
          res.render('expenses/add', {
            title: 'Edit an expense',
            expense: expenses.expenses[0],
            categories: categories,
            listId: lid
          });
        }
      });
      
    }
  });
};

/**
 * POST /expenses/:expenseId/edit
 * Display new expense form
 */
exports.postEditExpense = (req, res) => {
  const lid = req.params.listId;
  const eid = req.params.expenseId;
  // console.log(req.body)
  // console.log(req.file)

  var set = {
    "expenses.$.amount": req.body.amount, 
    "expenses.$.name": req.body.name, 
    "expenses.$.category": req.body.category
  }

  if(req.file && req.file.size > 0){
    var imageData = fs.readFileSync(req.file.path);
    var receipt = {contentType: req.file.mimetype, data: imageData};

    set["expenses.$.receipt"] = receipt

  }

  console.log(set)
  ExpenseList.update({_id: lid, 'expenses._id': eid }, 
    { "$set": set}, (err, list) =>{
      if (err) {
        req.flash('errors', { msg: err.message });
        res.redirect(`/lists/${lid}`);
      } else {
        req.flash('success', { msg: "Expense updated" });
        res.redirect(`/lists/${lid}`);
      }
  });
};

/**
 * GET /expenses/lists/:listId/collaborators/add
 * Display new expense form
 */
exports.getAddNewCollaborator = (req, res) => {
  const id = req.params.listId;
  User.find({}, 'email', (err, users) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      res.redirect(`/lists/${id}`);
    } else {
      ExpenseList.findOne({ _id: id }, (err, list) => {
        if (err) {
          req.flash('errors', { msg: err.message });
          res.redirect(`/lists/${id}`);
        } else {
          res.render('expenses/lists/collaborator', {
            title: 'Expenses',
            users,
            listId: id
          });
        }
      });
    }
  });
};

/**
 * GET /expenses/lists/:listId/collaborators
 * Display new expense form
 */
exports.postAddNewCollaborator = (req, res) => {

  const id = req.params.listId;
  let invitees = null;
  ExpenseList.findOne({ _id: id }, (err, list) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      res.redirect(`/lists/${id}`);
    } else {
      if (Array.isArray(req.body.users)) {
        invitees = req.body.users.length;
        req.body.users.forEach((element) => {
          list.collaborators.push(mongoose.Types.ObjectId(element));
        });
      } else {
        list.collaborators.push(mongoose.Types.ObjectId(req.body.users));
        invitees = 1;
      }

      list.save();
      req.flash('success', { msg: `${invitees} added to ${list.name}` });
      res.redirect(`/lists/${id}`);
    }
  });

  

};

exports.getDeleteExpense = (req, res) => {
  const lid = req.params.listId;
  const eid = req.params.expenseId;

  ExpenseList.findOne({ _id: lid, owner: req.user._id }, (err, list) => {
    if (err) {
      req.flash('errors', { msg: err.message });
    } else if (list != null ) {
      list.expenses.remove(eid);
      list.save();
      req.flash('success', { msg: 'successfully removed expense list' });
    } else {
      req.flash('errors', { msg: 'You\'re unauthorized to remove shared list' });
    }
    return res.redirect('/lists');
  });
};


