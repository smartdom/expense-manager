const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * GET /contact
 * Contact form page.
 */
exports.getContact = (req, res) => {
  const unknownUser = !(req.user);

  res.render('contact', {
    title: 'Contact',
    unknownUser,
  });
};

/**
 * POST /contact
 * Send a contact form via Nodemailer.
 */
exports.postContact = (req, res) => {
  let fromName;
  let fromEmail;
  if (!req.user) {
    req.assert('name', 'Name cannot be blank').notEmpty();
    req.assert('email', 'Email is not valid').isEmail();
  }
  req.assert('message', 'Message cannot be blank').notEmpty();

  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/contact');
  }

  if (!req.user) {
    fromName = req.body.name;
    fromEmail = req.body.email;
  } else {
    fromName = req.user.profile.name || '';
    fromEmail = req.user.email;
  }

  const mailOptions = {
    to: process.env.EMAIL_ADDR,
    from: `${fromName} <${fromEmail}>`,
    subject: 'Contact Form',
    text: req.body.message
  };

  sgMail.send(mailOptions, (err, result) =>{
    if (!err) {
      req.flash('success', { msg: 'Email has been sent successfully!' });
      res.redirect('/contact');
    } else {
      console.log('ERROR: Could not send email.\n', err);
      req.flash('warning', { msg: 'Error sending the message. Please try again shortly.' });
      return err;
    }
  });
};
