const _ = require('lodash');
const mongoose = require('mongoose');
const Category = require('../models/Category');
// var Expense = require('../models/Expense');
const ExpenseList = require('../models/ExpenseList');
const User = require('../models/User');

/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  if (req.user) {
    const userId = req.user._id;
    // get only _id, name and expenses collection, limit to 5, order by created date
    ExpenseList.find({}, 'expenses name _id')
      .or([{ owner: userId }, { collaborators: userId }]) .sort({'expenses.createdAt': -1}).slice('expenses', -5)
      .exec((err, list) => {
        if (err) {
          req.flash('errors', { msg: err.msg });
          res.redirect('/');
        } else {
          console.log(JSON.stringify(list))
          console.log(JSON.stringify(list.reverse()))
          res.render('homeok', {
            title: 'Home',
            lists: list.reverse()
          });
        }
      });
  } else {
    res.render('home', {
      title: 'Home'
    });
  }
};
