const _ = require('lodash');
const mongoose = require('mongoose');
const ExpenseList = require('../models/ExpenseList');
const User = require('../models/User');
const co = require('co');
const generate = require('node-chartist');


/**
 * GET /analytics/lists/:listId
 * Get list of expenses
 */
exports.getAnalyticsForList = (req, res) => {
  const id = req.params.listId;
  const userId = req.user._id;

  // We do a local aggregate on nested collection
  // TODO: Handle userId
  ExpenseList.aggregate([
    { $match: { _id: mongoose.Types.ObjectId(id) } },
    { $unwind: '$expenses' },
    {
      $group: {
        _id: '$expenses.category',
        amount: { $sum: '$expenses.amount' }
      }
    }
  ]).exec((err, result) => {

    co(function * () {
      var elWidth = (90 * result.length);

      // options object
      const options = {width: elWidth, height: 400};
      var data = {
        labels: [],
        series: [[]]
      };
      // data.series[0] = [];
      result.forEach(element => {
        data.labels.push(element._id+" ("+element.amount+")");
        data.series[0].push(element.amount);
      });
      
      const bar = yield generate('bar', options, data);
      return bar;
    
    }).then(function (value) {
      //TODO: Render view with properly formatted data
    res.render('expenses/lists/analytics', {
      title: 'Expenses',
      data: result,
      total: value,
      bargraph: value
    });
    }, function (err) {
      console.error(err.stack);
    });

    
  });
};
