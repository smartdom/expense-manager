var _ = require('lodash');
var User = require('../../models/User');

/**
 * GET /admin/users
 * 
 * Display users
 */
exports.getUsers = function (req, res) {
    User.find({}).sort({ createdAt: -1 }).exec(function (err, users) {
        if (err) {
            req.flash('errors', { msg: err.message });
            return res.redirect('/admin');
        } else {
            res.render('admin/users/list', {
                title: 'Users',
                users: users
            });
        }

    });
};

/**
 * GET /admin/users/:userId/roles
 * 
 * Display user roles
 */
exports.getEditRoles = function (req, res) {
    var userId = req.params.userId;
    User.findOne({ _id: userId }, function (err, user) {
        if (err) {
            req.flash('errors', { msg: err.message });
            res.redirect('/admin/users');
        } else {
            res.render('admin/users/roles', {
                title: 'Edytuj role',
                roles: user.roles,
                id: userId
            });
        }
    });
};

/**
 * POST /admin/users/:userId/roles
 * Update user roles
 */
exports.postEditRoles = function (req, res) {
    var id = req.body._id;
    var rolesString = req.body.roles;
    var rolesArray = rolesString.split(',');

    User.findById(id, function (err, user) {
        if (err) {
            req.flash('errors', { msg: 'Nie znaleziono użytkownika: ' + err.message });
            res.redirect('/admin/users');
        } else {
            user.roles = rolesArray;
            user.save(function (err) {
                if (err) {
                    req.flash('errors', { msg: 'Nie udało się zaktualizować ról: ' + err.message });
                    res.redirect('/admin/users');
                } else {
                    req.flash('success', { msg: 'Zaktualizowano role' });
                    res.redirect('/admin/users');
                }
            });
        }
    });
};

/**
 * GET /admin/users/:userId/delete
 * 
 * Delete user
 */
exports.deleteUser = function (req, res) {
    var id = req.params.userId;
    User.findOne({ _id: id }, function (err, user) {
        if (err) {
            req.flash('errors', { msg: 'Nie udało się usunąć użytkownika: ' + err.message });
            return res.render('admin/users');
        } else {
            user.remove();
            res.redirect('/admin/users');
        }
    });
}