var _ = require('lodash');
var Category = require('../../models/Category');


/**
 * GET /admin/categories
 * 
 * Display All categories
 */
exports.getCategories = function (req, res) {
    Category.find({}).sort({ createdAt: -1 }).exec(function (err, categories) {
        if (err) {
            req.flash('errors', { msg: err.message });
            return res.redirect('/admin');
        } else {
            res.render('admin/categories/list', {
                title: 'Categories',
                categories: categories
            });
        }

    });
};

/**
 * GET /admin/categories/new
 * 
 * Create new category
 */
exports.getNewCategory = function (req, res) {

    res.render('admin/categories/new', {
        title: 'Add new category'
    });
};


/**
 * POST /admin/categories
 * 
 * Create new category
 */
exports.postNewCategory = function (req, res) {

    var categoryName = req.body.name
    var category = new Category({name: categoryName});

    category.save(function(err){
        if (err) {
            req.flash('errors', { msg: err.message });
            return res.redirect('/admin/categories');
        } else {
            req.flash('success', { msg: 'successfully added new category' });
            return res.redirect('/admin/categories');
        }
    });
};

/**
 * GET /admin/categories/:categoryI
 * 
 * Display resume
 */
exports.getResumeDetails = function (req, res) {
    var resumeId = req.params.resumeId;
    Resume.findOne({ _id: resumeId }, function (err, resume) {
        if (err) {
            req.flash('errors', { msg: err.message });
            res.redirect('/admin/resumes');
        } else {
            if (resume.status == 1) {
                resume.status = statuses.opened;
                resume.save(function (error) {
                    if (err) {
                        req.flash('errors', { msg: 'Error while setting resume status: ' + err.message });
                        res.redirect('/admin/resumes/');
                    } else {
                        res.render('admin/linkedin/details', {
                            title: 'Resume details',
                            resume: resume
                        });
                    }
                });
            } else {
                res.render('admin/linkedin/details', {
                    title: 'Resume details',
                    resume: resume
                });
            }
        }
    });
}


/**
 * GET /admin/resumes/:resumeId/approve
 * 
 * Approve resume
 */
exports.getApproveResume = function (req, res) {
    this.setResumeStatus(req.params.resumeId, statuses.approved, req, res);
}

/**
 * GET /admin/resumes/:resumeId/reject
 * 
 * Reject resume
 */
exports.getRejectResume = function (req, res) {
    this.setResumeStatus(req.params.resumeId, statuses.rejected, req, res);
}

/**
 * GET /admin/categories/:categoryId/delete
 * 
 * Delete user
 */
exports.deleteResume = function (req, res) {
    var id = req.params.resumeId;
    Category.findOne({ _id: id }, function (err, category) {
        if (err) {
            req.flash('errors', { msg: 'Unable to delete category: ' + err.message });
            return res.render('admin/categories/list');
        } else {
            category.remove();
            req.flash('success', { msg: 'Resume removed' });
            res.redirect('/admin/resumes');
        }
    });
}

setResumeStatus = (resumeId, status, req, res) => {
    Resume.findOne({ _id: resumeId }, function (err, resume) {
        if (err) {
            req.flash('errors', { msg: 'Error while retrieving resume:' + err.message });
            return res.render('admin/users');
        } else {
            resume.status = status;
            resume.save(function (error) {
                if (err) {
                    req.flash('errors', { msg: 'Error while setting resume status: ' + err.message });
                    res.redirect('/admin/resumes/' + resumeId);
                } else {
                    res.redirect('/admin/resumes/');
                    req.flash('success', { msg: 'Resume updated' });
                }
            });
        }
    });
}

var statuses = Object.freeze({ new: 1, opened: 2, approved: 3, rejected: 4 });