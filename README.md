# expense-manager

Save, track and analyze your household expenses

## basic info

This project is based on [hackaton-starter](https://github.com/sahat/hackathon-starter) project. Kudos for the great boilerplate!

### requirements

- nodejs >= 8
- mongodb
- sendgrid account

### installation

After installing prerequisites above open a terminal in this folder and execute `npm install`

### staring the app

by default the app will try to load `.env` file which for obvious reasons is banned from the git repository.
Open the `.env.example` update it with your credentials and other needed data and rename it to `.env`

Use your preferred runner (`forever` or `nodemon` or whatever) and start `app.js`
